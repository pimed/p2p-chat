const topology = require('fully-connected-topology');
const jsonStream = require('duplex-json-stream');
const streamSet = require('stream-set');

const local = process.env.P2P_PORT || 5001;
const peers = process.env.PEERS ? process.env.PEERS.split(',') : [];

var server = topology(local, peers);
var streams = streamSet();
console.log(`Connected to ${local}`);

server.on('connection', (socket) => {
    console.log('New connection!');

    socket = jsonStream(socket);
    streams.add(socket);
    socket.on('data', (data) => {
        console.log(data.username + '> ' + data.message);
    });
});

process.stdin.on('data', (data) => {
    streams.forEach(socket => {
        socket.write({
            username: local,
            message: data.toString().trim()
        });
    });
});